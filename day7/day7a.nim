import strutils


proc processLine(s: string): bool =
  var
    first, second = ' '
    inHyper = false
    hasABBA = false

  for i in 0..s.len-3:
    first = s[i]
    second = s[i+1]
    if first == second:
      continue
    if first == '[':
      inHyper = true
      continue
    if first == ']':
      inHyper = false
      continue
    if s[i+2] == second and s[i+3] == first:
      hasABBA = true
      if inHyper:
        return false
  return hasABBA

proc proccessInput(f: string): seq =
  var 
    line = ""
    ips = newSeq[string]()

  let input = open(filename = f)
  while readline(input, line):
    if processLine(line):
      ips.add(line)

  return ips

var
  count = 0

let hasTLS = proccessInput("./input")

echo hasTLS.len

