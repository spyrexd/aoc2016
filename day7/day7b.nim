import strutils


proc processLine(s: string): bool =
  var
    first, second = ' '
    inHyper = false
    ABA, BAB = newSeq[string]()

  for i in 0..s.len-2:
    first = s[i]
    second = s[i+1]
    if first == second:
      continue
    if first == '[':
      inHyper = true
      continue
    if first == ']':
      inHyper = false
      continue
    if s[i+2] == first:
      if inHyper:
        BAB.add(s[i..i+2])
      else:
        ABA.add(s[i..i+2]) 

  for aba in ABA:
    for bab in BAB:
      first = aba[0]
      second = aba[1]
      if count(bab, first) == 1 and count(bab, second) == 2:
        echo aba," | ", bab
        return true

  return false

proc proccessInput(f: string): seq =
  var 
    line = ""
    ips = newSeq[string]()

  let input = open(filename = f)
  while readline(input, line):
    if processLine(line):
      ips.add(line)

  return ips

var
  count = 0

let hasTLS = proccessInput("./input")

echo hasTLS.len

