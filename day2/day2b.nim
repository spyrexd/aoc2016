type
  Row = array[5, char]

proc processLine(line: string): char =
  var
    pad: array[5, Row]
    ls = line
    row = 2
    col = 1

  pad[0] = ['X', 'X', '1', 'X', 'X']
  pad[1] = ['X', '2', '3', '4', 'X']
  pad[2] = ['5', '6', '7', '8', '9']
  pad[3] = ['X', 'A', 'B', 'C', 'X']
  pad[4] = ['X', 'X', 'D', 'X', 'X']

  for i in 0..<line.len:
    case line[i]
    of 'U':
      if row != 0 and pad[row - 1][col] != 'X':
        dec row
    of 'D':
      if row != 4 and pad[row + 1][col] != 'X':
        inc row
    of 'L':
      if col != 0 and pad[row][col - 1] != 'X':
        dec col
    of 'R':
      if col != 4 and pad[row][col + 1] != 'X':
        inc col
    else:
      discard()

  return pad[row][col]

var
  line = ""
  num = '\0'
let input = open(filename = "./input")
while readLine(f = input, line = line):
  num = processLine(line = line)
  stdout.write(num)
stdout.write("\n")
