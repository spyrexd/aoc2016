type
  Row = array[3, int]

proc processLine(line: string): int =
  var
    pad: array[3, Row]
    ls = line
    row, col = 1

  pad[0] = [1, 2, 3]
  pad[1] = [4, 5, 6]
  pad[2] = [7, 8, 9]

  for i in 0..<line.len:
    case line[i]
    of 'U':
      if row != 0:
        dec row
    of 'D':
      if row != 2:
        inc row
    of 'L':
      if col != 0:
        dec col
    of 'R':
      if col != 2:
        inc col
    else:
      discard()

  return pad[row][col]

var
  line = ""
  num = 0
let input = open(filename = "./input")
while readLine(f = input, line = line):
  num = processLine(line = line)
  stdout.write(num)
stdout.write("\n")
