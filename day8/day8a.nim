import strutils

const
  ROWS = 6
  COLS = 50
  ON_PIXEL = '#'
  OFF_PIXEL = '-'

type
  Screen = array[ROWS*COLS, char]

proc initScreen(s: var Screen) =
  for pixel in s.mitems:
    pixel = OFF_PIXEL

proc printScreen(s: Screen) =
  for i, pixel in s.pairs:
    if i mod COLS == 0 and i >= COLS:
      stdout.write("\n")
    stdout.write(pixel)
  stdout.write("\n")

proc countOnPixels(s: Screen): int =
  var count = 0
  for i in s:
    if i == ON_PIXEL:
      inc count
  return count

proc drawRect(w, h: int, s: var Screen) =
  var pos = 0
  for i in 0..<h:
    for j in 0..<w:
      s[pos+j] = ON_PIXEL
    inc(pos, 50)

proc rotate(a: var openArray[char], rotations: int) =
  var rot = rotations
  while rot > 0:
    var last = a[a.len-1]
    for i in countdown(a.len-1, 1):
      a[i] = a[i-1]
    a[0] = last
    dec rot

proc rotateCol(col, rotations: int, s: var Screen) =
  var 
    rotCol: array[ROWS, char]
    pos = col

  for i in 0..<ROWS:
    rotCol[i] = s[pos]
    inc(pos, COLS)

  rotate(rotCol, rotations)

  pos = col
  for i in 0..<ROWS:
    s[pos] = rotCol[i]
    inc(pos, COLS)

proc rotateRow(row, rotations: int, s: var Screen) =
  var
    rotRow: array[COLS, char]
    pos = row * COLS

  for i in 0..<COLS:
    rotRow[i] = s[pos+i]

  rotate(rotRow, rotations)

  for i in 0..<COLS:
    s[pos+i] = rotRow[i]

proc processFile(f: string, s: var Screen) =
  var line = ""

  let input = open(filename = f)
  while readLine(input, line):
    var command = splitWhitespace(line)
    case command[0]
    of "rect":
      var dem = split(command[1], 'x')
      drawRect(parseInt(dem[0]), parseInt(dem[1]), s)
    of "rotate":
      var rowCol = parseInt(split(command[2], '=')[1])
      var rots = parseInt(command[4])
      case command[1]:
      of "column":
        rotateCol(rowCol, rots, s)
      of "row":
        rotateRow(rowCol, rots, s)
      else: discard
    else: discard

var screen: Screen

initScreen(screen)
processFile("./input", screen)
printScreen(screen)
echo countOnPixels(screen)
