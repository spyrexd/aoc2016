import strutils

type
  Marker = tuple[length, times: int]


proc processMarker(s: string): Marker =
  var inner = split(s[1..^2], 'x')
  result.length = parseInt(inner[0])
  result.times = parseInt(inner[1])


let input = readFile("input")
var finalString = ""
var first, last, count = 0
var start_c, last_c = ' '
var marker: Marker

while first < input.len:
  start_c = input[first]
  case start_c:
  of '(':
    last = first
    last_c = input[first]
    count = 0
    while last_c != ')':
      inc(count)
      last_c = input[first + count]
    last = first + count
    marker = processMarker(input[first..last])
    first = last + 1
    if marker.length > 1:
      last = first + marker.length - 1
    else:
      last = first
    finalString.add(repeat(input[first..last], marker.times))
    first = last + 1
  else:
    finalString.add(start_c)
    inc(first)

echo finalString.len





