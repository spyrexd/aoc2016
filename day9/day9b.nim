import strutils

type
  Marker = tuple[length, times: int]


proc processMarker(s: string): Marker =
  var inner = split(s[1..^2], 'x')
  result.length = parseInt(inner[0])
  result.times = parseInt(inner[1])

proc processSubMarkerString(s: string): int =
  var first, last, count = 0
  var start_c, last_c = ' '
  var marker: Marker

  echo s

  while first < s.len:
    start_c = s[first]
    case start_c:
    of '(':
      last = first
      last_c = s[first]
      count = 0
      while last_c != ')':
        inc(count)
        last_c = s[first + count]
      last = first + count
      marker = processMarker(s[first..last])
      first = last + 1
      if marker.length > 1:
        last = first + marker.length - 1
      else:
        last = first
      inc(result, processSubMarkerString(s[first..last]) * marker.times)
      first = last + 1
    else:
      inc(first)
      inc(result)


let input = readFile("input")
echo processSubMarkerString(input)





