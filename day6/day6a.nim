import strutils

type
  Row = array['a'..'z', int]
  Table = array[8, Row]

proc processInput(filename: string): Table =
  var
    tab: Table
    line = ""

  let input = open(filename = filename)
  while readLine(input, line):
    for i, c in line.pairs:
      inc tab[i][c]
  return tab

proc maxIdx(row: Row): char =
  var
    idx = 'a'
    maxLetter = 0

  for i, c in row:
    if c > maxLetter:
      maxLetter = c
      idx = i

  return idx

var
  ans = ""

let tab = processInput("./input")
for row in tab:
  ans = ans & $maxIdx(row)

echo ans

