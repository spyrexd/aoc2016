import strutils

type
  Row = array['a'..'z', int]
  Table = array[8, Row]

proc processInput(filename: string): Table =
  var
    tab: Table
    line = ""

  let input = open(filename = filename)
  while readLine(input, line):
    for i, c in line.pairs:
      inc tab[i][c]
  return tab

proc minIdx(row: Row): char =
  var
    idx = 'a'
    maxLetter = high(int)

  for i, c in row:
    if c < maxLetter and c != 0:
      maxLetter = c
      idx = i

  return idx

var
  ans = ""

let tab = processInput("./input")
for row in tab:
  ans = ans & $minIdx(row)

echo ans

