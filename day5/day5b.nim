include md5
include strutils

var
  input = "ojvtpuvg"
  password: array[8, string]
  acc = 0
  hash: string  = ""
  pos = 0
  numSet = 0 
  asChar = ' '

proc printArray(a: array[8, string]) = 
  for c in a:
    stdout.write($c)
  stdout.write("\n")

password = [ "*", "*", "*", "*", "*", "*", "*", "*"]
while numSet <= 8:
  hash = getMD5(input & $acc)
  if hash[0..4] == "00000":
    echo numSet
    asChar = hash[5]
    if contains("01234567", $asChar):
      pos = parseInt($asChar)
      if password[pos] != "*":
        inc acc
        continue
      password[pos] = $hash[6]
      inc numSet
      printArray(password)
  inc acc

printArray(password)

