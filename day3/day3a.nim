import strutils


var
  line = " "
  numValid = 0
  sides: array[3, int]

let  input = open(filename = "./input")
while readLine(f = input, line = line):
  var i = 0
  for word in splitWhitespace(line):
    sides[i] = parseInt(word)
    inc i

  if sides[0] + sides[1] > sides[2]:
    if sides[0] + sides[2] > sides[1]:
      if sides[1] + sides[2] > sides[0]:
        inc numValid

echo numValid