import strutils

type
  Row = array[3, int]

proc printRow(row: Row) =
  echo row[0], " ,", row[1], " ,", row[2]

proc readFileContents(filename: string): seq =
  var
    line = ""
    lengths: Row
    sides = newSeq[Row]()

  let input = open(filename = filename)
  while readLine(input, line):
    var i = 0
    for word in splitWhitespace(line):
      lengths[i] = parseInt(word)
      inc i
    sides.add(lengths)
  return sides

proc validTri(sides: Row): bool =
  if sides[0] + sides[1] > sides[2]:
    if sides[0] + sides[2] > sides[1]:
      if sides[1] + sides[2] > sides[0]:
        return true
  return false

var
  row, col = 0
  tri: Row
  valid = 0

let sides = readFileContents("./input")
while col < 3:
  row = 0
  while row < sides.len:
    tri[0] = sides[row][col]
    tri[1] = sides[row + 1][col]
    tri[2] = sides[row + 2][col]
    printRow(tri)
    if validTri(tri):
      inc valid
    inc row, 3
  inc col

echo valid