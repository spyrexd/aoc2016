import strutils

type
  Room = tuple[name: string, secId: int, checksum: string]
  LetterCount = tuple[letter: char, count: int]
  Counts = array[26, LetterCount]

proc printRoom(room: Room) =
  echo room.name, ", ", room.secId, ", ", room.checksum

proc readFileContents(filename: string): seq =
  var
    line = ""
    room: Room
    rooms = newSeq[Room]()
    name, checksum = ""
    secId = 0

  let input = open(filename = filename)
  while readLine(input, line):
    var split = rsplit(line, '-', maxsplit=1)
    name = split[0]
    split = rsplit(split[1], '[', maxsplit=1)
    secId = parseInt(split[0])
    checksum = split[1]
    room = (name: name, secId: secId, checksum: checksum[0..checksum.len-2])
    printRoom(room)
    rooms.add(room)
  return rooms

proc sortCounts(counts: var Counts) =
  var swapped = true

  while swapped:
    swapped = false
    for i in 1..<counts.len:
      if counts[i-1].count < counts[i].count:
        swap(counts[i-1], counts[i])
        swapped = true

proc validateRoom(room: Room): bool =
  var
    count = 0
    counts:  Counts
    idx = 0
    checksum = ""

  for c in 'a'..'z':
    count = count(room.name, c)
    counts[idx] = (letter: c, count: count)
    inc idx

  sortCounts(counts)

  for i in 0..<5:
    checksum = checksum & counts[i].letter

  return checksum == room.checksum

var total = 0
let rooms = readFileContents("./input")
for room in rooms:
  if validateRoom(room):
    inc(total, room.secId)
echo total